FROM mcr.microsoft.com/dotnet/core/sdk:3.1-alpine AS builder
ARG GIT_TAG=master
RUN apk add --no-cache git patch
RUN git clone --branch ${GIT_TAG} --depth 1 https://github.com/jellyfin/jellyfin /build/jellyfin
ADD 0001-add-Hack-for-systemd-restart.patch /build/0001-add-Hack-for-systemd-restart.patch
WORKDIR /build/jellyfin
RUN patch -Np1 -i /build/0001-add-Hack-for-systemd-restart.patch
RUN dotnet publish --configuration Release --output=/app --self-contained --runtime alpine-x64 "-p:GenerateDocumentationFile=false;DebugSymbols=false;DebugType=none" Jellyfin.Server
RUN chmod 0755 /app/*.so /app/jellyfin && chmod 0644 /app/*.json /app/*.dll

FROM docker.io/node:14 AS builder-web
ARG GIT_TAG=master
RUN git clone --branch ${GIT_TAG} --depth 1 https://github.com/jellyfin/jellyfin-web /build/jellyfin-web
WORKDIR /build/jellyfin-web
RUN yarn && \
    yarn build:production

FROM docker.io/alpine:3.11
ENV DOTNET_RUNNING_IN_CONTAINER=true
LABEL   org.opencontainers.image.title="Jellyfin Media Server" \
        org.opencontainers.image.description="The Free Software Media System" \
        org.opencontainers.image.authors="Thomas Büttner" \
        org.opencontainers.image.url="quay.io/wuerfelbecher/jellyfin-alpine" \
        org.opencontainers.image.documentation="https://gitlab.com/Wuerfelbecher/jellyfin-alpine" \
        org.opencontainers.image.source="https://gitlab.com/Wuerfelbecher/jellyfin-alpine" \
        org.opencontainers.image.version=master
RUN apk add --no-cache libstdc++ libintl ffmpeg icu-libs libssl1.1
COPY --from=builder /app /app
COPY --from=builder-web /build/jellyfin-web/dist /app/jellyfin-web
EXPOSE 8096
WORKDIR /app
VOLUME [ "/data", "/Media" ]
HEALTHCHECK --interval=30s --timeout=30s --retries=3 --start-period=15s CMD wget -qO/dev/null http://localhost:8096/ || exit 1
ENTRYPOINT [ "/app/jellyfin", "--datadir=/data", "--configdir=/data/config", "--logdir=/data/logs", "--cachedir=/data/cache" ]
